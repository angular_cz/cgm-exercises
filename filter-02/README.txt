Filter-02

1. všimněte si limitTo
  - je zde 1400* záznamů měst, proto je výpis rovnou omezen na rozumnou hodnotu
  - jedná se o psč začínající číslicí 6 (jižní morava dle starého rozdělení)

  * toto je technologická úkázka, neznamená to, že by jste v reálné aplikaci měli filtrovat tisíce záznamu u klienta!

2. doplňte hledání
  - pole s ng-model reality.search už tu je, použijte jej ve filteru před limitTo

    ... zdrojový kód TODO 2.1 - doplňte hledání -->
                                                                                                             <div class="radio col-md-4"  ng-repeat="city in reality.cities| filter:reality.search | orderBy: reality.order | limitTo:30">

  - vyzkoušejte v hledacím poli zadání jméno města i psč

3. samostatné hledání podle typu

  - druhé pole, upravte tak, aby hledalo jen podle name

    ... zdrojový kód TODO 3.1 - hledání dle name -->
                                                                                                             <input type="text" class="form-control" ng-model="reality.search.name">

  - třetí pole, upravte tak, aby hledalo jen podle psč

    ... zdrojový kód TODO 3.2 - hledání dle psc -->
                                                                                                             <input type="text" class="form-control" ng-model="reality.search.psc">

  - původní pole upravte, aby stále hledalo ve všech polích - ($)

    ... zdrojový kód TODO 3.3 - hledání ve všech polích -->
                                                                                                             <input type="text" class="form-control" ng-model="reality.search.$">

4- řazení
  - použijte model radiobuttonů ve filteru orderBy

    ... zdrojový kód TODO 4.1 - filter orderBy -->
                                                                                                             <div class="radio col-md-4"  ng-repeat="city in reality.cities| filter:reality.search | orderBy: reality.order | limitTo:30">

  - upravte radiobutonny tak, aby řadily psč vzestupně a sestupně

    ... zdrojový kód TODO 4.2 - řazení dle psč -->
                                                                                                             <input type="radio" ng-model="reality.order" value="psc" />
                                                                                                             ...
                                                                                                             <input type="radio" ng-model="reality.order" value="-psc" />

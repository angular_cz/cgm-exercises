angular.module('moduleExample', [])
    .controller('UserController', function () {
      this.user = {
        name: 'Petr',
        surname: 'Novák',
        yearOfBirth: 1982
      };

      this.getAge = function () {
        // TODO odkomentujte
        //console.log('getAge');
        var now = new Date();
        return now.getFullYear() - this.user.yearOfBirth;
      };
    });

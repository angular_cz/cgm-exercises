'use strict';

angular.module('directiveApp', [])

  .controller('RatingCtrl', function () {
    this.restaurant = {
      personal: 2,
      food: 3,
      place: 4
    };
  })
  
  .directive("rating", function () {
    return {
      transclude: true,
      restrict: "A",
      templateUrl: "rating.html",
      require: "?^averageRating",
      
      scope: {
        rating: '='
      },
      link: function (scope, element, attr, controller) {
        controller.addRating(scope.rating);

        scope.hasStar = function (starNumber) {
          return starNumber <= scope.rating;
        };
      }
    };
  })

  .directive("averageRating", function (averageCounter) {
    return {
      restrict: "A",
      transclude: true,
      templateUrl: "average-rating.html",
      controller : function() {
        this.addRating = function(rating) {
          averageCounter.addRating(rating);
        };
        
        this.getAverage = function() {
          return averageCounter.getAverage();
        };
      },
      controllerAs: "average"
    };
  })
  
  .service("averageCounter", function () {
    var count = 0;
    var sum = 0;

    this.addRating = function (rating) {
      count++;
      sum += parseInt(rating);
    };

    this.getAverage = function () {
      if (count === 0) {
        return 0;
      }
      
      return Math.round((sum / (count * 5)) * 100);      
    };
  });

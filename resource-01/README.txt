Resource-01

1. Do definice modulu přidejte závislost ngResource

2. Inicilizace factory používající $resource
  - ve factory orders inicializujte $resource pro url v konstantě
  - jako url použijte : REST_URI + '/orders/:id
  - parametr id namapujte na @id

    ... zdrojový kód TODO 2 - inicializujte $resource -->
                                                                                                            return $resource(REST_URI + '/orders/:id', {"id": "@id"});
3. Načtení dat
  - v controlleru OrderListControlleru
  - použijte vytvořenou factory orders
  - pomocí volání query() naplňte this.orders

    ... zdrojový kód TODO 3 - načtení dat -->
                                                                                                            this.orders = orders.query();
  - vyzkoušejte, že se data načtou a podívejte se v konzoli na komunikaci se serverem
  - všimněte si probliknutí prázdné tabulky
  - zamyslete se proč tomu tak je

4. Získání dat pro detail
  - data pro detail získejte už v definici routy v sekci resolve pro orderData
  - pro získání využijte orders.get s předáním id
  - návratovou hodnotou funkce orderData by mělo být promise - použijte .$promise

    ... zdrojový kód TODO 4 - načtení dat pro detail -->
                                                                                                            return orders.get({'id' : id}).$promise;

    - vyzkoušejte přechod na detail a podívejte se v konzoli na komunikaci se serverem

5. Vytvoření nového objektu
  - v controlleru OrderCreateController
  - vytvořte nový objekt pomocí operátoru new a factory orders
  - přiřaďte jej do this.order
    ... zdrojový kód TODO 5.1 - vytvoření nového objektu -->
                                                                                                            this.order = new orders();

  - v metodě save(order) uložte vytvářený objekt pomocí volání jeho metody $save
  - přesměrování $location.path by se mělo provést až po uložení
    ... zdrojový kód TODO 5.2 - uložení nového objektu -->
                                                                                                            this.save = function(order) {
                                                                                                              order.$save(function() {
                                                                                                                $location.path("/detail/" + order.id);
                                                                                                              });
                                                                                                            }

  - vyzkoušejte vytvoření nového záznamu a podívejte se na komunikaci se serverem
  - zamyslete se, kdy došlo k naplnění id objektu

6. Změna záznamu
  - v metodě updateOrder(order) uložte order pomocí $save();
    ... zdrojový kód TODO 6 - uložení záznamu -->
                                                                                                            this.updateOrder = function (order) {
                                                                                                              order.$save();
                                                                                                            };

  - vzkoušejte funkčnost změny stavu a podívejte se v konzoli na komunikaci se serverem
  - vyzkoušejte změnu stavu na "odeslaná"
  - zamyslete se, kde se vzalo ve view datum

7. Smazání záznamu
  - ov metodě removeOrder(order) odstraňte záznam pomocí volání $remove
  - fyzické odstranění ze seznamu by se mělo provést až po volání $remove
    ... zdrojový kód TODO 7 - odstranění záznamu -->
                                                                                                            this.removeOrder = function (order) {
                                                                                                              order.$remove(function () {
                                                                                                                var index = orderCtrl.orders.indexOf(order);
                                                                                                                orderCtrl.orders.splice(index, 1);
                                                                                                              });
                                                                                                            };

  - vyzkoušejte funkčnost mazání a podívejte se v konzoli na komunikaci se serverem

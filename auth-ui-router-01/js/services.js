angular.module('authApp')

    .factory('Orders', function(REST_URI, $resource) {
      return $resource(REST_URI + '/orders/:id', {"id": "@id"});
    })

    .service('loginModal', function($modal) {

      this.modalInstance = null;

      /**
       * @private
       */
      this.createModalInstance_ = function() {
        this.modalInstance = $modal.open({
          templateUrl: 'authModal.html',
          controller: 'AuthCtrl',
          controllerAs: 'auth'
        });

        var self = this;

        this.modalInstance.result.then(
            function(data) {
              delete self.modalInstance;
              return data;
            },
            function() {
              delete self.modalInstance;
            });
      };

      /**
       * @pulic
       * @return {Promise}
       */
      this.getLoginModal = function() {
        if (!this.modalInstance) {
          this.createModalInstance_();
        }

        return this.modalInstance.result;
      };

    })

    .service('authService', function($http, $rootScope, REST_URI) {
      this.user = null;

      /**
       * @param {string} name
       * @param {string} pass
       * @return {Promise}
       */
      this.login = function(name, pass) {
        return $http.post(REST_URI + "/login", {name: name, password: pass})
            .then(this.processLoginResponse_.bind(this));
      };

      /**
       * @private
       */
      this.processLoginResponse_ = function(response) {
        if (response.status === 200) {
          this.user = response.data;
          $http.defaults.headers.common['X-Auth-Token'] = response.data.token;

          $rootScope.$broadcast('authService:loginSuccessfull', this.user);

          return this;
        } else {
          $rootScope.$broadcast('authService:loginFailed');
        }
      };

      this.logout = function() {
        $http.get(REST_URI + "/logout");
        delete this.user;
        delete $http.defaults.headers.common['X-Auth-Token'];
        $rootScope.$broadcast('authService:logout');
      };

      /**
       * @return {Boolean}
       */
      this.isLogged = function() {
        return Boolean(this.user);
      };

      /**
       * @param {string} role
       * @return {Boolean}
       */
      this.hasRole = function(role) {
        if (!this.isLogged()) {
          return false;
        }

        return this.user.roles.indexOf(role) !== -1;
      };

      /**
       * @param {array} roles
       * @return {Boolean}
       */
      this.hasSomeRole = function(roles) {
        if (!this.isLogged()) {
          return false;
        }

        return roles.some(this.hasRole.bind(this));
      };
    });
  
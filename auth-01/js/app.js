angular.module('authApp', ['ngRoute', 'ngResource', 'ui.bootstrap'])
  .constant('REST_URI', 'http://api-angularcz.rhcloud.com/security-api')

  .config(function ($routeProvider) {

    $routeProvider

      .when('/orders', {
        templateUrl: 'orderList.html',
        controller: 'OrderListController',
        controllerAs: 'list'
      })

      .when('/detail/:id', {
        templateUrl: 'orderDetail.html',
        controller: 'OrderDetailController',
        controllerAs: 'detail',
        resolve: {
          orderData: function (Orders, $route) {
            var id = $route.current.params.id;

            return Orders.get({'id': id}).$promise;
          }
        }
      })

      .when('/create', {
        templateUrl: 'orderCreate.html',
        controller: 'OrderCreateController',
        controllerAs: 'create',
        resolve: {
          orderData: function (Orders) {
            return new Orders();
          }
        }
      })

      .when('/', {
        templateUrl: 'home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })

      .otherwise('/');
  })

  .run(function ($rootScope, $location) {
    $rootScope.$on('login:accessDenied', function () {
      $location.path("/")
    })
  })

  .run(function ($rootScope, $location) {
    // TODO 3.2 - reakce na zprávu login:loggedOut
  });




